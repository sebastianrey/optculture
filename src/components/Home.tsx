import * as React from "react"
import { Link } from "react-router-dom"

const Home: React.SFC = () => (
  <div>
    <h1>You can only improve what you can measure.</h1>
    <h2>
      Gain insight from 22 key health markers.
      <br /> Personalized score to show you how to improve.
      <br /> Recommendations tailored to your score.
    </h2>
    <Link to="/login">Log In </Link>
  </div>
)

export default Home
