import * as React from "react"
import { Route, Switch } from "react-router-dom"

// import { connect } from "react-redux"
// import { Redirect, RouteProps } from "react-router"

import LoginPage from "./auth/Login"
import Home from "./Home"

const App: React.SFC = () => (
  <div>
    <Switch>
      <Route exact={true} path="/" component={Home} />
      <Route path="/login" component={LoginPage} />
    </Switch>
  </div>
)
export default App
