import * as React from "react"
import { Link } from "react-router-dom"

const LoginPage: React.SFC = props => (
  <section className="row content_register_kit">
    <article className="ui">
      <div className="content_form">
        <h2>Sign In</h2>
        <form>
          <div className="content_information">
            <div className="grid">
              <div className="row">
                <div className="wide column">
                  <label className="row">Username</label>
                  <input
                    className="row ui fluid input"
                    placeholder="Username"
                    type="email"
                  />
                </div>
              </div>
              <div className="row">
                <div className="wide column">
                  <label className="row">Password</label>
                  <input
                    className="row ui fluid input"
                    placeholder="Password"
                    type="password"
                  />
                </div>
              </div>
            </div>
            <br />
            <button
              className="ui blue large"
              // onClick={this.submitLoginForm}
              // disabled={status.type === "Logging in"}
            >
              <Link to="/">Sign In </Link>
            </button>
          </div>
        </form>
      </div>
    </article>
  </section>
)

export default LoginPage
