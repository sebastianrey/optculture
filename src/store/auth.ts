import * as api from "api"

import { push } from "connected-react-router"
import { SagaIterator } from "redux-saga"
import { call, put, takeEvery } from "redux-saga/effects"
import { Action, actionCreatorFactory } from "typescript-fsa"
import { reducerWithInitialState } from "typescript-fsa-reducers"
import { done, failed, loading, notAsked, RemoteData } from "utils/remote-data"

// // Str.

export type Status =
  | { type: "Not logged in" }
  | { type: "Logging in" }
  | {
      type: "Login failed"
    } & api.ErrorResponse
  | {
      type: "Logged in"
    } // & api.LoginResponse
  | {
      type: "Loggin out"
    }

export type Model = {
  status: Status
  signupRequestStatus: RemoteData<string>
  // settingsDataRequestStatus: RemoteData<api.ProfileResponseBody>
}

export const initialState: Model = {
  status:
    localStorage.getItem("authStatus") !== null
      ? JSON.parse(localStorage.getItem("authStatus") as string)
      : { type: "Not logged in" },
  signupRequestStatus: notAsked(),
  // settingsDataRequestStatus: notAsked(),
}

// Act.

const mkAction = actionCreatorFactory("Auth")

export const logIn = mkAction.async<
  {}, // api.LoginRequest,
  string, // api.LoginResponseBody,
  api.APIError[]
>("LOG_IN")
export const logOut = mkAction.async<{}, {}, {}>("LOG_OUT")
export const settingsData = mkAction.async<
  {},
  string,
  string
  // api.ProfileResponseBody,
  // api.APIError[]
>("SETTINGS_DATA")

// // Reducer

export const reducer = reducerWithInitialState<Model>(initialState)
  .case(logIn.started, (state, _) => ({
    ...state,
    status: { type: "Logging in" },
  }))
  .case(logIn.failed, (state, { error }) => ({
    ...state,
    status: { type: "Login failed", errors: error },
  }))
  .case(logIn.done, (state, { result }) => ({
    ...state,
    status: { type: "Logged in" },
  }))
  .build()
// .case(logOut.started, (state, _) => ({
//   ...state,
//   status: { type: "Loggin out" },
// }))
// .case(logOut.done, (state, _) => ({
//   ...state,
//   status: { type: "Not logged in" },
// }))
// .case(settingsData.started, (state, _) => ({
//   ...state,
//   settingsDataRequestStatus: loading(),
// }))
// .case(settingsData.failed, (state, { error }) => ({
//   ...state,
//   settingsDataRequestStatus: failed(error),
// }))
// .case(settingsData.done, (state, { result }) => ({
//   ...state,
//   settingsDataRequestStatus: done(result),
//   status: { type: "Logged in", data: result },
// }))

// // Sagas

export function* rootSaga(): SagaIterator {
  yield takeEvery(logIn.started, logInSaga)
  // yield takeEvery(logOut.started, logOutSaga)
  // yield takeEvery(settingsData.started, profileDataSaga)
}

function* logInSaga(action: Action<string>): SagaIterator {
  try {
    const resp: api.Response<string> = yield call(api.login, action.payload)
    if ("errors" in resp) {
      yield put(
        logIn.failed({
          params: action.payload,
          error: resp.errors,
        }),
      )
      return
    }
    const newStatus: Status = {
      type: "Logged in",
      ...resp,
    }
    localStorage.setItem("authStatus", JSON.stringify(newStatus))
    yield put(
      logIn.done({
        params: action.payload,
        result: resp.data,
      }),
    )
    yield put(push("/"))
  } catch (e) {
    yield put(
      logIn.failed({
        params: action.payload,
        error: [
          {
            status: 0,
            title: "Request failed",
            detail:
              "Something happened trying to make a network request, please try again later",
          },
        ],
      }),
    )
  }
}
