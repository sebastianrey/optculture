import { combineReducers } from "redux"
import { all, fork } from "redux-saga/effects"
import * as auth from "store/auth"

export type Store = {
  auth: auth.Model
}

export const rootReducer = combineReducers<Store>({ auth: auth.reducer })

export function* rootSaga() {
  yield all([fork(auth.rootSaga)])
}
