let domain = window.location.hostname

domain =
  window.location.hostname === "localhost" ||
  window.location.hostname === "127.0.0.1"
    ? "127.0.0.1:3000"
    : domain

const apiUrl = domain.includes("127.0.0.1")
  ? `http://127.0.0.1:3000/api`
  : `/api`

export interface DataResponse<T> {
  data: T
}

export type APIErrorSource = { pointer: string } | { parameter: string }

export interface APIError {
  status: number
  title: string
  detail: string
  source?: APIErrorSource
}

export interface ErrorResponse {
  errors: APIError[]
}

export type Response<T> = DataResponse<T> | ErrorResponse

async function http<RespBody>(
  url: string,
  opts: RequestInit & {
    body?: undefined
    payload?: any
    headers?: Headers
  },
): Promise<Response<RespBody>> {
  const { headers = new Headers(), payload, ...restOpts } = opts
  headers.append("Content-Type", "application/json")
  const resp = await fetch(apiUrl + url, {
    body: payload !== undefined ? JSON.stringify(payload) : undefined,
    headers,
    credentials: "include",
    ...restOpts,
  })
  return resp.json()
}

// LOGIN //
export interface LoginRequest {
  email: string
  password: string
}

export interface LoginResponseBody {
  company: {
    id: number
    name: string
  }
}
export type LoginResponse = DataResponse<string>

// export const login = (payload: LoginRequest) =>
//   http<string>("/login", { method: "post", payload })

export const login = () => http<string>("/login", {})
